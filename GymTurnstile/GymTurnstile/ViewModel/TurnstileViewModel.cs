﻿using GymTurnstile.Helper;
using GymTurnstile.Management;
using GymTurnstile.Model;
using GymTurnstile.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace GymTurnstile.ViewModel
{
    public class TurnstileViewModel : BaseViewModel
    {
        #region Fields
        private ObservableCollection<Message> _logs;
        private readonly IGymTurnstile _gymTurnstileM = GymTurnstileManager.Instance;
        #endregion Fields

        #region Property
        public ObservableCollection<Message> Log
        {
            get { return _gymTurnstileM.getLog(); }
            set { _gymTurnstileM.SettLog(value); }
        }
        public ICommand AddCommand { get; }
        public ICommand AddRandomCommand { get; }
        public string? Firstname { get; set; }
        public string? Lastname { get; set; }

        #endregion Property

        #region Constructor
        public TurnstileViewModel() { 
            _logs = new ObservableCollection<Message>();
            AddCommand = new RelayCommand(o => AddInGym());
            AddRandomCommand = new RelayCommand(o => AddRandomInGym());
        }
        #endregion Constructor

        #region Methods

        private void AddInGym()
        {
            if (Firstname == default || Lastname == default) MessageBox.Show("Firstname and Lastname can not be null");
            else
            {
                _gymTurnstileM.Add(
                       // User.getUserTask(Firstname, Lastname, _gymTurnstileM)
                       new UserDTO() { FirstName = Firstname, LastName = Lastname}
                    );
              /*  _logs.Add(Message.getMessageByType(Message.MessageType));*/
            }
        }

        private void AddRandomInGym()
        {
            var firstname = Faker.Name.First();
            var laststname = Faker.Name.Last();
            _gymTurnstileM.Add(
                       //User.getUserTask(firstname, laststname, _gymTurnstileM)
                       new UserDTO() { FirstName = firstname, LastName = laststname }
                   );

        }
        
       /* public void initLogMessage()
        {
            for (int i = 0; i < 10; i++)
            {
                _logs.Add(new Message()
                {
                    text = Faker.Name.FullName(),
                    timestamp = DateTime.UtcNow
                });
            }
        }*/

        #region Helper
       // private Message notifyEnterMessage() => new Message() { text= $"User {Firstname} {Lastname} Entered the Gym !", timestamp = DateTime.UtcNow };
        #endregion Helper
        #endregion Methods
    }
}

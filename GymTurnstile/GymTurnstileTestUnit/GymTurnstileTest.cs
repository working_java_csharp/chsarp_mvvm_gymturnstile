using GymTurnstile.Services;
using GymTurnstile.Management;
using GymTurnstile.Model;

namespace GymTurnstileTestUnit
{
    public class GymTurnstileTest
    {
        private readonly IGymTurnstile _gymObserver = GymTurnstileManager.Instance;

        [Fact]
        public void Test1()
        {
            Parallel.For(0, 15, i =>
            {
                //var utask = User.getUserTask(, , _gymObserver);
                _gymObserver.Add(new UserDTO() { FirstName = Faker.Name.First(), LastName = Faker.Name.Last()});
            });

            Assert.True(_gymObserver.countT() <= 10);
        }
    }
}